<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.list');
    }

    public function getData()
    {
        $profile = Profile::with('users')
            ->select('profiles.*');

        return Datatables::of($profile)
            ->addColumn('action', function ($obj) {
                return view('profile.action', compact('obj'))->render();
            })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profile.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // lakukan validasi


        try {
            $profile = new Profile;

            $profile->user_id               = auth()->user()->id;
            $profile->alamat_ktp            = $request->alamat_ktp;
            $profile->pekerjaan             = $request->pekerjaan;
            $profile->nama_lengkap          = $request->nama_lengkap;
            $profile->pendidikan_terakhir   = $request->pendidikan_terakhir;
            $profile->nomor_telpon          = $request->nomor_telepon;

            // dd($profile);

            $profile->save();
        } catch (exception $e) {
        }
        return redirect('profile')->with('status', 'Profile save!');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::findOrFail(1);
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $profile = Profile::find($id);
            $profile->user_id               = auth()->user()->id;
            $profile->alamat_ktp            = $request->alamat_ktp;
            $profile->pekerjaan             = $request->pekerjaan;
            $profile->nama_lengkap          = $request->nama_lengkap;
            $profile->pendidikan_terakhir   = $request->pendidikan_terakhir;
            $profile->nomor_telpon          = $request->nomor_telepon;
            // dd($profile);
            $profile->save();
        } catch (exception $e) {
        }

        return redirect('profile')->with('status', 'Profile updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::find($id);

        $profile->delete();
        return redirect('profile')->with('status', 'Profile delete!');
    }
}
