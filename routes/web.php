<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile/add', 'ProfileController@create')->name('profile.add');
Route::get('/profile/getprofile', 'ProfileController@getData')->name('profile.getprofile');
Route::get('/profile/edit/{id}', 'ProfileController@edit')->name('profile.edit');
Route::post('/profile/save', 'ProfileController@store')->name('profile.save');
Route::put('/profile/update/{id}', 'ProfileController@update')->name('profile.update');
Route::delete('/profile/delete/{id}', 'ProfileController@destroy')->name('profile.delete');
// Route::get('/home', 'HomeController@index')->name('home');
