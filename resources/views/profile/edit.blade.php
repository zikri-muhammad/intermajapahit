@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        <form action="{{ route('profile.update', [$profile->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Alamat Ktp</label>
                                <input type="text" class="form-control" value="{{ $profile->alamat_ktp }}"
                                    name="alamat_ktp" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Pekerjaan</label>
                                <input type="text" class="form-control" value="{{ $profile->pekerjaan }}" name="pekerjaan"
                                    id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama Lengka</label>
                                <input type="text" class="form-control" value="{{ $profile->nama_lengkap }}"
                                    name="nama_lengkap" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Pendidikan Terakhir</label>
                                <input type="text" class="form-control" value="{{ $profile->pendidikan_terakhir }}"
                                    name="pendidikan_terakhir" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nomor Telepon</label>
                                <input type="text" class="form-control" value="{{ $profile->nomor_telpon }}"
                                    name="nomor_telepon" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
