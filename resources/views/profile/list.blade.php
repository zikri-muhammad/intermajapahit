@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        <h1>List Profile</h1>

                        <table class="table" id="table_profile">
                            <thead>
                                {{-- <th scope="col">#</th> --}}
                                <th scope="col">username</th>
                                <th scope="col">email</th>
                                <th scope="col">alamat ktp</th>
                                <th scope="col">pekerjaan</th>
                                <th scope="col">nama lengkap</th>
                                <th scope="col">pendidikan terakhir</th>
                                <th scope="col">Nomor Telpon</th>
                                <th scope="col">action</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@push('script')
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script>
        jQuery(document).ready(function() {
            Main.init();
            // Index.init();
        });
    </script>
    <script>
        $(function() {
            // alert('ok')
            const url = '{{ route('profile.getprofile') }}';
            $.noConflict();
            $('#table_profile').DataTable({
                "bSort": true,
                "bProcessing": true,
                "bServerSide": true,
                "ajax": {
                    "url": url,
                    "type": "get",
                },
                dom: 'Bfrtip',
                columns: [
                    // { data: 'id',searchable:true},
                    {
                        data: 'users.name'
                    },
                    {
                        data: 'users.email',
                        searchable: true
                    },
                    {
                        data: 'alamat_ktp',
                        searchable: true
                    },
                    {
                        data: 'pekerjaan',
                        searchable: true
                    },
                    {
                        data: 'nama_lengkap',
                        searchable: true
                    },
                    {
                        data: 'pendidikan_terakhir',
                        searchable: true
                    },
                    {
                        data: 'nomor_telpon',
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }

                ]

            });


        });
    </script>
@endpush
