<div class="btn-group">
    <a href="{{ route('profile.edit', [$obj->id]) }}" class="btn btn-sm btn-primary">edit</i></a>

    <form method="POST" action="{{ route('profile.delete', [$obj->id]) }}" style="display: inline-table">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-sm btn-danger delete-user">delete</button>
    </form>
</div>

